/*Exercise 1*/
/*try catch варто є сенс використовувати коли ми отримуємо дані
 від сервера або коли користувач вводить якісь дані, щоб не крашнувся увесь скрипт ми можемо
 схопити помилку відслідити її дати користувачеві зрозуміти що сталося і в скрипті прописати що робити у разі помилки,
  який код можна виконувати далі */




/*Exercise 2*/
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
]

const showList = function (listOfBooks) {
    let placeForList = document.getElementById('root')
    let list = createDomEl('ul', placeForList)
    return listOfBooks.map(book => {
        (function () {
            let requiredProperties = ['author', 'name', 'price']
            try {
                requiredProperties.forEach(property => {
                    if (!(property in book)) {
                        throw new SyntaxError(`Missing ${property} property in ${listOfBooks.indexOf(book)} book`)
                    }
                })
                createDomEl('li', list, 'list-item')
                for (let key in book) {
                    list.lastChild.style.cssText = 'border: 2px solid grey; font-size: 30px; padding: 10px: margin: 5px'
                    list.lastChild.insertAdjacentHTML('beforeend', `<p>${book[key]}</p>`);
                }
            } catch (err) {
                console.log(err.message)
            }
        })()
    })
}

function createDomEl (element, attachmentPlace, elClass) {
    attachmentPlace.append(document.createElement(element))
    if(!elClass) {
        return document.querySelector(`${element}`)
    } else {
        return  document.querySelector(`${element}`).classList.add(elClass)
    }

}
showList(books)


